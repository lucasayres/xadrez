# Engenharia de Software #

**Trabalho:** Aplicar os padrões de projeto no jogo de xadrez

**Aluno:** Lucas Dantas Gama Ayres

**Professor:** Cláudio Nogueira Sant’Anna

### 1. Objetivo ###

Dado um sistema, aplicar nele padrões de projeto visando melhorar a qualidade de seu projeto.
O sistema que será dado ao aluno será um jogo de xadrez feito. Além disso, será fornecido também ao aluno um diagrama de classes com a indicação dos padrões de projetos que deverão ser implementados.

### 2. Entrega ###

O código do trabalho deve ser enviado por email para “santanna@dcc.ufba.br” até o dia 18/05/2016.

### 3. Metodologia ###

**O trabalho é individual.**

Passos do projeto:

1) Em primeiro lugar o aluno deverá estudar o código fonte dado pelo professor e o diagrama de classes.

2) O diagrama de classes será explicado, brevemente pelo professor.
Identificar as partes do projeto que deverão ser modificadas.

3) Modificar o código, aplicando padrões de projeto, produzindo assim, um novo projeto.

4) Alguns novas funcionalidades devem ser implementadas. Esses novas funcionalidades permitirão a
aplicação de algum padrão de projeto.

**Observações:**

É obrigatório o uso do ambiente de programação Netbeans 8.0.2 ou superior

É obrigatório o uso da versão mais recente do JDK.

### 4. Método e Critérios para a avaliação ###

A avaliação será feita em forma de arguição. O professor pedirá ao aluno que apresente o sistema e explique diversas partes do código, assim como, mostre como ele foi feito. Os critérios para avaliação serão:

Avaliação                               | Pontuação
----------------------------------------|------------
**Arguição**                            | 
Funcionamento do novo sistema           | 2 pontos
Implementação dos padrões de projeto    | 8 pontos
**Total**                               | 10 pontos

### 5. Funcionalidades do sistema ###

**RF001 – Número de participante no jogo**

**Descrição:** O sistema deve possibilitar que duas pessoas possam jogar uma contra a outra.

**Regra de negócios:**

* O jogo deve pedir os dois nomes dos jogadores.
* Os nomes devem ter pelo menos 4 caracteres.

**RF002 – Movimentação das peças do xadrez**

**Descrição:** O sistema deve possibilitar que as peças de xadrez sejam movimentadas pelo jogador segundo as regras clássicas do xadrez ou segundo o modo estratégico (ver RF007).
**Regra de negócios:**

* O sistema não deve permitir que o jogador movimente peças adversárias

**RF003 – Iluminação das possíveis jogadas**

**Descrição:** O sistema deve, após a peça ser clicada, iluminar em verde as possíveis jogadas que aquela peça possa fazer.

**Regra de negócios:**

* O sistema não deve permitir que o jogador jogue com as peças adversárias
* O jogador só pode movimentar peças para os locais iluminados

**RF004 – Tempo de jogo**

**Descrição:** O sistema deve mostrar o tempo de duração do jogo na mesma tela do jogo (como um cronômetro).

**RF005 – Pausar o jogo**

**Descrição:** O sistema deve possibilitar ao jogador pausar o jogo em qualquer momento.

**Regra de negócios:**

* Ao pausar o jogo o sistema deve desabilitar as peças de xadrez, ou seja, nenhum jogador pode realizar nenhuma jogada.
* Ao pausar o jogo o sistema deve pausar o relógio do jogo (tempo de jogo – RF004).

**RF006 – Continuar o jogo depois de ele ser pausado**

**Descrição:** O sistema deve possibilitar ao jogador continuar o jogo depois dele ser pausado.

**Regra de negócios:**

* Ao continuar o jogo o sistema deve habilitar as peças de xadrez, ou seja, os jogadores podem voltar a realizar jogadas.
* Ao continuar o jogo o sistema deve habilitar o relógio do jogo (tempo de jogo – RF004).

**RF007 – Modo normal e modo estratégico**

**Descrição:** O sistema deve possibilitar aos jogadores escolher, antes da partida começar, o modo em que o jogo irá operar: modo normal ou modo estratégico.

* Modo normal – As peças se movimentam como no xadrez clássico.
* Modo estratégico – Quando uma peça capturar outra peça, a primeira deverá assumir as
características da segunda. Ex: Se o bispo branco captura o cavalo preto, deste ponto em diante o bispo branco deverá se movimentar como um cavalo.

**RF008 – Operação Undo**
**Descrição:** O sistema deve possibilitar aos jogadores retornar **uma** jogada (**operação undo**).