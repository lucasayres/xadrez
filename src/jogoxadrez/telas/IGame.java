/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jogoxadrez.telas;

import jogoxadrez.state.AbstractGame;

/**
 *
 * @author lucasayres
 */
public interface IGame {
    public void play(AbstractGame game);
    public AbstractGame getGameInstance(String game);
}
