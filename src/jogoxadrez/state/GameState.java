package jogoxadrez.state;

public interface GameState {
   public void paused(AbstractGame state);
   public void inprogress(AbstractGame state);
   public void finished(AbstractGame state);
}