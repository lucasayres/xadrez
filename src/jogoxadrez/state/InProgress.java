package jogoxadrez.state;

import javax.swing.JOptionPane;
import jogoxadrez.telas.Chess;

public class InProgress implements GameState {
    public void paused(AbstractGame state) {
        state.setEstado(new Paused());
    }

    public void inprogress() {
        System.out.println("O jogo já está em andamento");
    }

    public void finished() {
        System.out.println("O jogo ainda não foi finalizado");
    }

    public void inprogress(AbstractGame state) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void finished(AbstractGame state) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
