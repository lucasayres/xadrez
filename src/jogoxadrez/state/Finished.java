package jogoxadrez.state;

import jogoxadrez.telas.Chess;

public class Finished implements GameState {    
    private Chess fp;

    public void paused(AbstractGame state) {
        //NADA
    }

    public void inprogress(AbstractGame state) {
        //NADA
    }

    public void finished(AbstractGame state) {
        //Pausar cronometro
            //TODO
        fp.terminarJogo();
        state.setEstado(new InProgress());
    }
    
}
