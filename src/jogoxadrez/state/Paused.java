package jogoxadrez.state;

import javax.swing.JOptionPane;

public class Paused implements GameState{

    public void paused(AbstractGame state) {
        System.out.println("O jogo já está pausado");
    }

    public void inprogress(AbstractGame state) {
        state.setEstado(new InProgress());
    }

    public void finished(AbstractGame state) {
        System.out.println("O jogo ainda não foi finalizado");
    }
    
}