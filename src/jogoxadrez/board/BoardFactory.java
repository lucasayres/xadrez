/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jogoxadrez.board;

import jogoxadrez.telas.Chess;
import jogoxadrez.telas.ChessBoard;

/**
 *
 * @author lucasayres
 */
public class BoardFactory {
    public ChessBoard getBoard(Chess fp, String board){
        if (board.equals("chess")){
            return new ChessBoard(fp);
        }
        return null;
    }
}
